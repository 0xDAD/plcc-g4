# plcc-g4

## Overview

IEC 61131 based language ANTLRv4 grammar

## QuickStart

### Build and Run

### Run tests

In  VS Code terminal type:

1. `cd java-out`
2. `javac ST*.java`
3. `grun ST pouDecl  ..\tests\simple_types.st -tokens`

### Visual Studio Code

#### Debugging

        {
            "type": "antlr-debug",
            "request": "launch",
            "input": "tests/6-oscat-basic-333.st",
            "grammar": "STParser.g4",
            "startRule": "translationUnitDecl",
            "name": "Run Parser",
            "printParseTree": true,
            "visualParseTree": true
        }