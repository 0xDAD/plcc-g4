for k := 0 to N/8 DO
   rl1 := cos(2.0 * 3.14159265359 * uint_to_real(k) / uint_to_real(N) );
   rl2  := sin(2.0 * 3.14159265359 * uint_to_real(k) / uint_to_real(N) );
end_for;