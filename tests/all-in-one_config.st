//BASIC-NOT: error

FUNCTION_BLOCK fb_DO8 
VAR_INPUT inQ : BOOL ; END_VAR 

VAR 
	boolFireOdd : BOOL := FALSE ; 
	iCntr : INT := 0 ; 
	boolRun : BOOL := TRUE ; 
END_VAR

	VAR doChannel1_Pin1 AT %Q* : BOOL ; END_VAR 
	VAR doChannel2_Pin5 AT %Q* : BOOL ; END_VAR 
	VAR doChannel3_Pin2 AT %Q* : BOOL ; END_VAR 
	VAR doChannel4_Pin6 AT %Q* : BOOL ; END_VAR 
	VAR doChannel5_Pin3 AT %Q* : BOOL ; END_VAR 
	VAR doChannel6_Pin7 AT %Q* : BOOL ; END_VAR 
	VAR doChannel7_Pin4 AT %Q* : BOOL ; END_VAR 
	VAR doChannel8_Pin8 AT %Q* : BOOL ; END_VAR 


iCntr := iCntr + BOOL_TO_INT(inQ);

boolFireOdd := BOOL_TO_INT(MOD(iCntr, 2));

doChannel1_Pin1 := boolRun AND boolFireOdd;
doChannel4_Pin6 := boolRun AND boolFireOdd;
doChannel5_Pin3 := boolRun AND boolFireOdd;
doChannel8_Pin8 := boolRun AND boolFireOdd;

doChannel2_Pin5 := boolRun AND NOT boolFireOdd;
doChannel3_Pin2 := boolRun AND NOT boolFireOdd;
doChannel6_Pin7 := boolRun AND NOT boolFireOdd;
doChannel7_Pin4 := boolRun AND NOT boolFireOdd;

 
END_FUNCTION_BLOCK; 

FUNCTION_BLOCK fb_DO4 
VAR_INPUT inQ : BOOL ; END_VAR 
VAR boolRun : BOOL := TRUE ; END_VAR 
VAR iState : INT := 0 ; END_VAR 
VAR VAR doChannel1_Pin1 AT %Q* : BOOL := FALSE ; END_VAR 
VAR VAR doChannel2_Pin5 AT %Q* : BOOL := FALSE ; END_VAR 
VAR VAR doChannel3_Pin4 AT %Q* : BOOL := FALSE ; END_VAR 
VAR VAR doChannel4_Pin8 AT %Q* : BOOL := FALSE ; END_VAR 
iState := iState + BOOL_TO_INT(inQ);

doChannel1_Pin1 := boolRun AND EQ(iState, 1);
doChannel2_Pin5 := boolRun AND EQ(iState, 2);
doChannel3_Pin4 := boolRun AND EQ(iState, 3);
doChannel4_Pin8 := boolRun AND EQ(iState, 4);

IF (iState >= 5) THEN
	iState := 0;
END_IF; 
END_FUNCTION_BLOCK 
; 
FUNCTION_BLOCK fb_LedAndSwitches 
VAR boolRun : BOOL := TRUE ; END_VAR 
VAR diChannel1_Pin1 AT %I* : BOOL ; END_VAR 
VAR diChannel2_Pin5 AT %I* : BOOL ; END_VAR 
VAR diChannel3_Pin4 AT %I* : BOOL ; END_VAR 
VAR diChannel4_Pin8 AT %I* : BOOL ; END_VAR 
VAR doChannel1_Pin1 AT %Q* : BOOL ; END_VAR 
VAR doChannel2_Pin5 AT %Q* : BOOL ; END_VAR 
VAR doChannel3_Pin4 AT %Q* : BOOL ; END_VAR 
VAR doChannel4_Pin8 AT %Q* : BOOL ; END_VAR 
VAR tSetTimeFast : TIME := t#1s ; END_VAR 
VAR tSetTimeSlow : TIME := t#3s ; END_VAR 
VAR tOnCounterFast : TON ; END_VAR 
VAR tOffCounterFast : TON ; END_VAR 
VAR tOnCounterSlow : TON ; END_VAR 
VAR tOffCounterSlow : TON ; END_VAR 
VAR boolRunningFast : BOOL := TRUE ; END_VAR 
VAR boolRunningSlow : BOOL := TRUE ; END_VAR 

doChannel1_Pin1 := NOT diChannel2_Pin5;

doChannel4_Pin8 := diChannel4_Pin8;

// -------------------- start timers
boolRun := boolRun AND tSetTimeFast > t#0s AND tSetTimeSlow > t#0s;

tOnCounterFast (IN := boolRun AND NOT tOffCounterFast.Q, PT := tSetTimeFast);
tOffCounterFast (IN := boolRun AND tOnCounterFast.Q, PT := tSetTimeFast);

boolRunningFast := boolRun AND NOT tOnCounterFast.Q AND diChannel3_Pin4;

tOnCounterSlow (IN := boolRun AND NOT tOffCounterSlow.Q, PT := tSetTimeSlow);
tOffCounterSlow (IN := boolRun AND tOnCounterSlow.Q, PT := tSetTimeSlow);

boolRunningSlow := boolRun AND NOT tOnCounterSlow.Q AND NOT diChannel3_Pin4;

doChannel3_Pin4 := boolRunningFast OR boolRunningSlow;

 
END_FUNCTION_BLOCK 
; 
FUNCTION_BLOCK fb_Demo 
VAR tOnCounter0 : TON ; END_VAR 
VAR boolExecute : BOOL ; END_VAR 
VAR boolRun : BOOL := TRUE ; END_VAR 
VAR tSwitchTime0 : TIME := t#0.02s ; END_VAR 
VAR tSwitchTime1 : TIME := t#0.5s ; END_VAR 
VAR tOnCounter1 : TON ; END_VAR 
VAR fbDO8a : fb_DO8 ; END_VAR 
VAR fbDO8b : fb_DO8 ; END_VAR 
VAR fbDO4a : fb_DO4 ; END_VAR 
VAR fbDO4b : fb_DO4 ; END_VAR 
VAR fbLedAndSwitches : fb_LedAndSwitches ; END_VAR 
// Run Demos

fbLedAndSwitches();

tOnCounter0(IN := boolRun AND NOT tOnCounter0.Q, PT := tSwitchTime0);
fbDO8a(inQ:=tOnCounter0.Q);
fbDO8b(inQ:=tOnCounter0.Q);

tOnCounter1(IN := boolRun AND NOT tOnCounter1.Q, PT := tSwitchTime1);
fbDO4a(inQ:=tOnCounter1.Q);
fbDO4b(inQ:=tOnCounter1.Q);

// ------------------------------------------------ 
END_FUNCTION_BLOCK 
; 

CONFIGURATION IECConfig 
RESOURCE SPS2016 ON ControlPC 
VAR_GLOBAL tSetCycleTime : TIME := t#10ms ; END_VAR 
TASK Task1 ( INTERVAL:= tSetCycleTime , PRIORITY:= 0 ); 
PROGRAM Demo WITH Task1 : fb_Demo ; 
END_RESOURCE 
VAR_CONFIG 
SPS2016.Demo.fbDO8b.doChannel1_Pin1 AT %QX1.12.0.1 : BOOL ; 
END_VAR 
END_CONFIGURATION 
