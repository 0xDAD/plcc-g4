(* StFFT - Fast fourier transform on IEC-61131-3 Structured text
   https://sourceforge.net/projects/stfft/
   Tamitech Automation Oy - 2013
   Jaakko Pasanen
   jaakko.pasanen@tamitech.fi
   
   Licenced under LGPLv3 and commercial licence,
   for commercial licencing contact tamitech@tamitech.fi
*)
   
N := 2;
while N <= L do
        (* Twidddle factor precalculation *)
        for k := 0 to N/8 DO
            w_real[k] := cos(2.0 * 3.14159265359 * uint_to_real(k) / uint_to_real(N) );
            w_complex[k] := sin(2.0 * 3.14159265359 * uint_to_real(k) / uint_to_real(N) );
        end_for;
        
        ind := 0;
        w_ind := 0;
        span := 1;
        LpN := L/N;
        for k := 0 to L-1 do
            
            (* Selecting right twiddle factor
               Cases are split in two just to halve condition evaluations *)
            if w_ind <= N/2 then
                if w_ind <= N/8 then
                    w_r := w_real[ w_ind ];
                    w_c := -w_complex[ w_ind ];
                elsif w_ind <= N/4 then
                    w_r := w_complex[ N/4-w_ind ];
                    w_c := -w_real[ N/4-w_ind ];
                elsif w_ind <= 3*N/8 then
                    w_r := -w_complex[ w_ind-N/4 ];
                    w_c := -w_real[ w_ind-N/4 ];
                else
                    w_r := -w_real[ N/2-w_ind ];
                    w_c := -w_complex[ N/2-w_ind ];
                end_if;
            else
                if w_ind <= 5*N/8 then
                    w_r := -w_real[ w_ind-N/2 ];
                    w_c := w_complex[ w_ind-N/2 ];
                elsif w_ind <= 3*N/4 then
                    w_r := -w_complex[ 3*N/4-w_ind ];
                    w_c := w_real[ 3*N/4-w_ind ];
                elsif w_ind <= 7*N/8 then
                    w_r := w_complex[ w_ind-3*N/4 ];
                    w_c := w_real[ w_ind-3*N/4 ];
                else
                    w_r := w_real[ N-w_ind ];
                    w_c := w_complex[ N-w_ind ];
                end_if;
            end_if;
            
            (* Summation of two elements, with odd element multipication with twiddle
               A + B*w = (A_r+iA_c)+(B_r+iB_c)(W_r+iW_c)
			*)
            A_r := x_real[ ind ];
            A_c := x_complex[ ind ];
            B_r := x_real[ ind+LpN ];
            B_c := x_complex[ ind+LpN ];
            
            (* Separation of real and imaginary components *)
            tmp_real[ k ] := A_r + B_r*w_r - B_c*w_c;
            tmp_complex[ k ] := A_c + B_r*w_c + B_c*w_r;
            
            (* Indexing
               Index is the index of even element. Odd element is always with index
               offset of LpN from even element. *)
            ind := ind + 1;
            if ind + LpN = L then (* Transition from first half to second *)
                ind := 0;
                w_ind := w_ind + 1;
                span  := 1;
            elsif ind + LpN = span*LpN*2 then (* Transition between spans *)
                ind := ind + LpN;
                w_ind := w_ind + 1;
                span := span + 1;
            end_if;

        end_for;
        N := N*2;
        
        (* Copy all elements from temp to data vector *)
        for k := 0 to L-1 do
            x_real[ k ] := tmp_real[ k ];
            x_complex[ k ] := tmp_complex[ k ];
        end_for;
end_while;
